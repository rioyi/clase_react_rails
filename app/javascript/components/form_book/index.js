import React from 'react'

class FormBook extends React.Component {
  constructor(props) {
    super(props);
    
  }

  render() {
    return (
      <div>
        <input onChange={this.props.onInputChange} placeholder="title" name="title" type="text"/>
        <input onChange={this.props.onInputChange} placeholder="author" name="author" type="text"/>
        <input onChange={this.props.onInputChange} placeholder="description" name="description" type="text"/>
        <input onChange={this.props.onInputChange} placeholder="kind" name="kind" type="text"/>
        <button onClick={this.props.handleSubmit}>Submit</button>        
      </div>
    )
  }
}

export default FormBook;